# step-project-forkio

## Name
Step Project "Forkio"

## Description
This project was done as the first group step project of the Frontend course from DAN.IT education. 
List of project used technologies: HTML, CSS, JavaScript, Git, DOM, Chrome DevTools, GULP, SASS.
The project was divided into two tasks for developers.
Task 1: Maksym Salenko,
Task 2: Anna Sargina.

## Authors and acknowledgment
Maksym Salenko,
Anna Sargina.