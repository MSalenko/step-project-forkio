const {src, dest, watch, parallel, series} = require('gulp');
const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require("gulp-imagemin")
const clean = require('gulp-clean');

function styles() {
    return src('src/scss/*.scss')
    .pipe(autoprefixer({ overrideBrowserslist: ['last 3 versions'], cascade: true}))
    .pipe(concat('style.min.css'))
    .pipe(scss({ outputStyle: 'compressed' }))
    .pipe(dest('dist/css'))
    .pipe(browserSync.stream())
}

function scripts() {
    return src('src/js/*.js')
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(dest('dist/js'))
    .pipe(browserSync.stream())
}

function images() {
    return src('src/img/*.*')
    .pipe(imagemin())
    .pipe(dest('dist/img'))
}

function browsersync() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
}

function watching() {
    watch(['src/scss/*.scss'], styles)
    watch(['src/js/*.js'], scripts)
    watch(['src/img/*.*'], images)
    watch(['index.html']).on('change', browserSync.reload); 
}

function cleanDist() {
    return src('dist/**/*.*')
    .pipe(clean())
}

exports.styles = styles;
exports.scripts = scripts;
exports.browsersync = browsersync;
exports.watching = watching;

exports.dev = parallel(styles, scripts, images, browsersync, watching);
exports.build = series(cleanDist, styles, scripts, images);