const dropBtn = document.querySelector('.logo__menu-btn');
const dropMenu = document.querySelector('.nav__menu-container');
const menuLogo = document.querySelector('.logo__menu-icon');
const dropdownMenuArr = [dropMenu, menuLogo, menuLogo.nextElementSibling];

const dropdownMenu = () => dropdownMenuArr.forEach(el => el.classList.toggle('inactive'));

dropBtn.addEventListener('click', dropdownMenu);